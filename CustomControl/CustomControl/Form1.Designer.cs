﻿namespace CustomControl
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelZ1 = new PanelZ.PanelZ();
            this.radioButtonZ2 = new RadioButtonZ.RadioButtonZ();
            this.radioButtonZ1 = new RadioButtonZ.RadioButtonZ();
            this.labelZ6 = new LabelZ.LabelZ();
            this.labelZ5 = new LabelZ.LabelZ();
            this.labelZ4 = new LabelZ.LabelZ();
            this.labelZ3 = new LabelZ.LabelZ();
            this.labelZ2 = new LabelZ.LabelZ();
            this.labelZ1 = new LabelZ.LabelZ();
            this.buttonZ1 = new ButtonZ.ButtonZ();
            this.panelZ1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelZ1
            // 
            this.panelZ1.Controls.Add(this.radioButtonZ2);
            this.panelZ1.Controls.Add(this.radioButtonZ1);
            this.panelZ1.Controls.Add(this.labelZ6);
            this.panelZ1.Controls.Add(this.labelZ5);
            this.panelZ1.Controls.Add(this.labelZ4);
            this.panelZ1.Controls.Add(this.labelZ3);
            this.panelZ1.Controls.Add(this.labelZ2);
            this.panelZ1.Controls.Add(this.labelZ1);
            this.panelZ1.Controls.Add(this.buttonZ1);
            this.panelZ1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZ1.EndColor = System.Drawing.Color.MediumOrchid;
            this.panelZ1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.panelZ1.ForeColor = System.Drawing.Color.Black;
            this.panelZ1.GradientAngle = 90;
            this.panelZ1.Location = new System.Drawing.Point(0, 0);
            this.panelZ1.Name = "panelZ1";
            this.panelZ1.Size = new System.Drawing.Size(677, 469);
            this.panelZ1.StartColor = System.Drawing.Color.SlateBlue;
            this.panelZ1.TabIndex = 0;
            this.panelZ1.Transparent1 = 200;
            this.panelZ1.Transparent2 = 150;
            // 
            // radioButtonZ2
            // 
            this.radioButtonZ2.BoxLocation_X = 0;
            this.radioButtonZ2.BoxLocation_Y = 0;
            this.radioButtonZ2.BoxSize = 20;
            this.radioButtonZ2.DisplayText = "Nữ";
            this.radioButtonZ2.EndColor = System.Drawing.Color.Yellow;
            this.radioButtonZ2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.radioButtonZ2.ForeColor = System.Drawing.Color.White;
            this.radioButtonZ2.GradientAngle = 90;
            this.radioButtonZ2.Location = new System.Drawing.Point(317, 267);
            this.radioButtonZ2.MouseHoverColor1 = System.Drawing.Color.Red;
            this.radioButtonZ2.MouseHoverColor2 = System.Drawing.Color.DarkOrange;
            this.radioButtonZ2.Name = "radioButtonZ2";
            this.radioButtonZ2.Size = new System.Drawing.Size(78, 34);
            this.radioButtonZ2.StartColor = System.Drawing.Color.ForestGreen;
            this.radioButtonZ2.TabIndex = 10;
            this.radioButtonZ2.TabStop = true;
            this.radioButtonZ2.Text = "Nữ";
            this.radioButtonZ2.TextLocation_X = 25;
            this.radioButtonZ2.TextLocation_Y = 0;
            this.radioButtonZ2.Transparent1 = 150;
            this.radioButtonZ2.Transparent2 = 150;
            this.radioButtonZ2.UseVisualStyleBackColor = true;
            // 
            // radioButtonZ1
            // 
            this.radioButtonZ1.BoxLocation_X = 0;
            this.radioButtonZ1.BoxLocation_Y = 0;
            this.radioButtonZ1.BoxSize = 20;
            this.radioButtonZ1.DisplayText = "Nam";
            this.radioButtonZ1.EndColor = System.Drawing.Color.Yellow;
            this.radioButtonZ1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.radioButtonZ1.ForeColor = System.Drawing.Color.White;
            this.radioButtonZ1.GradientAngle = 90;
            this.radioButtonZ1.Location = new System.Drawing.Point(174, 267);
            this.radioButtonZ1.MouseHoverColor1 = System.Drawing.Color.Red;
            this.radioButtonZ1.MouseHoverColor2 = System.Drawing.Color.DarkOrange;
            this.radioButtonZ1.Name = "radioButtonZ1";
            this.radioButtonZ1.Size = new System.Drawing.Size(80, 34);
            this.radioButtonZ1.StartColor = System.Drawing.Color.ForestGreen;
            this.radioButtonZ1.TabIndex = 9;
            this.radioButtonZ1.TabStop = true;
            this.radioButtonZ1.Text = "Nam";
            this.radioButtonZ1.TextLocation_X = 25;
            this.radioButtonZ1.TextLocation_Y = 0;
            this.radioButtonZ1.Transparent1 = 150;
            this.radioButtonZ1.Transparent2 = 150;
            this.radioButtonZ1.UseVisualStyleBackColor = true;
            // 
            // labelZ6
            // 
            this.labelZ6.AutoSize = true;
            this.labelZ6.BackColor = System.Drawing.Color.Transparent;
            this.labelZ6.DisplayText = "SDT :";
            this.labelZ6.EndColor = System.Drawing.Color.DarkBlue;
            this.labelZ6.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelZ6.ForeColor = System.Drawing.Color.Transparent;
            this.labelZ6.GradientAngle = 45;
            this.labelZ6.Location = new System.Drawing.Point(28, 231);
            this.labelZ6.Name = "labelZ6";
            this.labelZ6.Size = new System.Drawing.Size(58, 22);
            this.labelZ6.StartColor = System.Drawing.Color.Red;
            this.labelZ6.TabIndex = 6;
            this.labelZ6.Text = "SDT :";
            this.labelZ6.Transparent1 = 255;
            this.labelZ6.Transparent2 = 255;
            // 
            // labelZ5
            // 
            this.labelZ5.AutoSize = true;
            this.labelZ5.BackColor = System.Drawing.Color.Transparent;
            this.labelZ5.DisplayText = "Email :";
            this.labelZ5.EndColor = System.Drawing.Color.DarkBlue;
            this.labelZ5.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelZ5.ForeColor = System.Drawing.Color.Transparent;
            this.labelZ5.GradientAngle = 45;
            this.labelZ5.Location = new System.Drawing.Point(28, 192);
            this.labelZ5.Name = "labelZ5";
            this.labelZ5.Size = new System.Drawing.Size(68, 22);
            this.labelZ5.StartColor = System.Drawing.Color.Red;
            this.labelZ5.TabIndex = 5;
            this.labelZ5.Text = "Email :";
            this.labelZ5.Transparent1 = 255;
            this.labelZ5.Transparent2 = 255;
            // 
            // labelZ4
            // 
            this.labelZ4.AutoSize = true;
            this.labelZ4.BackColor = System.Drawing.Color.Transparent;
            this.labelZ4.DisplayText = "Giới tính :";
            this.labelZ4.EndColor = System.Drawing.Color.DarkBlue;
            this.labelZ4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelZ4.ForeColor = System.Drawing.Color.Transparent;
            this.labelZ4.GradientAngle = 45;
            this.labelZ4.Location = new System.Drawing.Point(28, 267);
            this.labelZ4.Name = "labelZ4";
            this.labelZ4.Size = new System.Drawing.Size(92, 22);
            this.labelZ4.StartColor = System.Drawing.Color.Red;
            this.labelZ4.TabIndex = 4;
            this.labelZ4.Text = "Giới tính :";
            this.labelZ4.Transparent1 = 255;
            this.labelZ4.Transparent2 = 255;
            // 
            // labelZ3
            // 
            this.labelZ3.AutoSize = true;
            this.labelZ3.BackColor = System.Drawing.Color.Transparent;
            this.labelZ3.DisplayText = "Password :";
            this.labelZ3.EndColor = System.Drawing.Color.DarkBlue;
            this.labelZ3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelZ3.ForeColor = System.Drawing.Color.Transparent;
            this.labelZ3.GradientAngle = 45;
            this.labelZ3.Location = new System.Drawing.Point(28, 154);
            this.labelZ3.Name = "labelZ3";
            this.labelZ3.Size = new System.Drawing.Size(100, 22);
            this.labelZ3.StartColor = System.Drawing.Color.Red;
            this.labelZ3.TabIndex = 3;
            this.labelZ3.Text = "Password :";
            this.labelZ3.Transparent1 = 255;
            this.labelZ3.Transparent2 = 255;
            // 
            // labelZ2
            // 
            this.labelZ2.AutoSize = true;
            this.labelZ2.BackColor = System.Drawing.Color.Transparent;
            this.labelZ2.DisplayText = "ID :";
            this.labelZ2.EndColor = System.Drawing.Color.DarkBlue;
            this.labelZ2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelZ2.ForeColor = System.Drawing.Color.Transparent;
            this.labelZ2.GradientAngle = 45;
            this.labelZ2.Location = new System.Drawing.Point(28, 111);
            this.labelZ2.Name = "labelZ2";
            this.labelZ2.Size = new System.Drawing.Size(41, 22);
            this.labelZ2.StartColor = System.Drawing.Color.Red;
            this.labelZ2.TabIndex = 2;
            this.labelZ2.Text = "ID :";
            this.labelZ2.Transparent1 = 255;
            this.labelZ2.Transparent2 = 255;
            // 
            // labelZ1
            // 
            this.labelZ1.AutoSize = true;
            this.labelZ1.BackColor = System.Drawing.Color.Transparent;
            this.labelZ1.DisplayText = "Form Đăng Ký";
            this.labelZ1.EndColor = System.Drawing.Color.DarkBlue;
            this.labelZ1.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelZ1.ForeColor = System.Drawing.Color.Transparent;
            this.labelZ1.GradientAngle = 45;
            this.labelZ1.Location = new System.Drawing.Point(195, 42);
            this.labelZ1.Name = "labelZ1";
            this.labelZ1.Size = new System.Drawing.Size(189, 31);
            this.labelZ1.StartColor = System.Drawing.Color.Red;
            this.labelZ1.TabIndex = 1;
            this.labelZ1.Text = "Form Đăng Ký";
            this.labelZ1.Transparent1 = 255;
            this.labelZ1.Transparent2 = 255;
            // 
            // buttonZ1
            // 
            this.buttonZ1.BackColor = System.Drawing.Color.Transparent;
            this.buttonZ1.BorderColor = System.Drawing.Color.Transparent;
            this.buttonZ1.BorderWidth = 2;
            this.buttonZ1.ButtonShape = ButtonZ.ButtonZ.ButtonsShapes.Rect;
            this.buttonZ1.ButtonText = "";
            this.buttonZ1.EndColor = System.Drawing.Color.DarkTurquoise;
            this.buttonZ1.FlatAppearance.BorderSize = 0;
            this.buttonZ1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonZ1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonZ1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonZ1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.buttonZ1.ForeColor = System.Drawing.Color.Transparent;
            this.buttonZ1.GradientAngle = 90;
            this.buttonZ1.Location = new System.Drawing.Point(483, 403);
            this.buttonZ1.MouseClickColor1 = System.Drawing.Color.Red;
            this.buttonZ1.MouseClickColor2 = System.Drawing.Color.Yellow;
            this.buttonZ1.MouseHoverColor1 = System.Drawing.Color.Turquoise;
            this.buttonZ1.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.buttonZ1.Name = "buttonZ1";
            this.buttonZ1.ShowButtontext = true;
            this.buttonZ1.Size = new System.Drawing.Size(92, 42);
            this.buttonZ1.StartColor = System.Drawing.Color.DarkBlue;
            this.buttonZ1.TabIndex = 0;
            this.buttonZ1.Text = "Đăng ký";
            this.buttonZ1.TextLocation_X = 29;
            this.buttonZ1.TextLocation_Y = 19;
            this.buttonZ1.Transparent1 = 250;
            this.buttonZ1.Transparent2 = 250;
            this.buttonZ1.UseVisualStyleBackColor = false;
            this.buttonZ1.Click += new System.EventHandler(this.buttonZ1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 469);
            this.Controls.Add(this.panelZ1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panelZ1.ResumeLayout(false);
            this.panelZ1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private PanelZ.PanelZ panelZ1;
        private ButtonZ.ButtonZ buttonZ1;
        private LabelZ.LabelZ labelZ1;
        private LabelZ.LabelZ labelZ6;
        private LabelZ.LabelZ labelZ5;
        private LabelZ.LabelZ labelZ4;
        private LabelZ.LabelZ labelZ3;
        private LabelZ.LabelZ labelZ2;
        private RadioButtonZ.RadioButtonZ radioButtonZ1;
        private RadioButtonZ.RadioButtonZ radioButtonZ2;
    }
}

